# Pythoon_AI_Application_Sharing

This is used to share some app developed by python. Feel free to use , it's only for research sharing only. Thanks

---


### Outline

- ja_app_auto_scan_slot_and_data_collect_with_image_classfication:
    - windows_os: auto_scan_slot_game:  ![](image/ja_app_auto_scan_demo.mp4)
    - windows_os: image_classification_into_database.



- ja_app_auto_record_screen_by_9_languages
    - support macos_os: 
        - ![Double click the application:](image/auto_record/01_click.jpg)

        - ![Select language:](image/auto_record/02_Select_language.jpg)

        - ![Type work_dir:](image/auto_record/03_dir.jpg) 

        - move mouse to the four conrner ( Top-Left -> Top-Right -> Botton-Right -> Botton-Left) and key-in the position value
        ![](image/auto_record/04_left_top.jpg)
        - ![Type Enter to start recording](image/auto_record/05_run.jpg)
        
        - ![Type 'q' to finish recording](image/auto_record/06_type_q_to_stop.jpg)
        - Check the video:

    - Suggestion purpose (research used only .. )
        - ![record_short_video (the time-frame is not syn. ):](image/auto_record_demo_02.mp4)
            
        - ![record your tutorial for demo yourown coding-function to your team:](image/auto_record_demo_01.mp4)
            
        
    - will support windows soon:

- ja_app_train_yourown_image_by_Keras_TF 
    - Now Support Windows's OS
    - 01_windows_set_language.exe 
        - Used to set yourown language !
    - 02_windows_create_sample_one.exe
        - Used to create the trainning sample 
    - 03_windows_create_sample_two.exe
        - Used to create the validation sample 
    - 04_windows_create_index.exe
        - create the csv files for mapping the image_object under sample folder 
    - 05_windows_train.exe
        - start to training the data and make .h5 model files
    - 06_windows_predict.exe
        - start to make prediction for new images
    - ![](image/prediction_demo.jpg)

Current , base on the .h5 files , delopers can load the .h5 files and make their own prediction coding by themself for the templer time. Just google how to load the h5 files, and there you go ! will provide the <span color="blue" > 06_predict.exe soon </span> 



- ja_chinese_remainder_theorem
    - support automatically calculate the formula by input the number like [ 5, 6, 7, 11] 



![](image/tf_keras.jpg)

### History
---
- 19122x- Updated 1st Version 
- 200103- Add auto_record_screen_application
- 200107- Add ja_app_train_yourown_image_by_Keras_TF 
- 200108- Add 06_windows_prediction_to ja_app_train_xxxxxxxxx 
     (Close the app implemnt task, will start sharing new app )😈
