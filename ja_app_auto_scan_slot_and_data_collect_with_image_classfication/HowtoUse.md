
Step 1:  In sample folder

put any image.jpg you need to classfu

---

Step 2:  Clean the files under train_models and run:
- windows_create_sample_one.exe
- windows_create_sample_two.exe


---

Step 3:  Creat the model index.csv file by run:
- windows_index.exe

---

Step 4: Clean the .h5 files and train new model by run:
- windows_train.exe


---

Step 5: Put the SLOT Slot with run_1.jpg, run_2.jpg etc...

(Or you can auto scan the slot by run windows_auto_slot.py, I will make a English version and Japanese version later.. in one week)


---

Step 6: Run the prediction and collect the image data into pandas pickle file under temp_windows folder by run:

- windows_collect.exe

---

Step 7: Check the pickle file under temp_windows folder and run the quick summary by run:
- windows_summary.exe  (I will make the English and Japanese version as well... later...)

---


### How to use Auto Slot EXE
---

Step 1: Open the web slot game you want to scan

Step 2: Check the scan area, the changed area, the spin button area location.
    - scan area:  program will scan the 4 poinst (Left-Top, Right-Top, Right-Botton, Left-Botton) you key in.
    - changed area:  it's the area each spin end, which changed color for example.  The program will take the changed event as a trigger, to save each run into run_1.png , run_2.png run by run.
    - spin button mouse, key in the pos , so that the program know how to click

Step 3: Run windows_auto_slot.exe

Step 4: 
- Use the monitor value , use Crl_C  , to key in the scan-area data (4 points one by one)
- Use the monitor value , use Crl_C  , to key in the chan-area data (4 points one by one)
- Use the monitor value , use Crl_C  , to key in the mouse click pos (X,Y)
- Set the waitting time for the program to click the button (for example , 9 Seconds)
- Set how many runs you want to scan, for example 100 runs.
- Press Enter when you are ready.

Step 5: Process the scan slot.
- After finish the scan, the program will automatically covert the run_1.png to run_1.jpg for further trainning use
- You can us eother 3rd tool to save the run_1.jpg ,run_2.jpg by yourself into work_tem folder.

---