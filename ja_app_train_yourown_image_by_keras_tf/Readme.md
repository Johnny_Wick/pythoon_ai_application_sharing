## Image classificaton application.
---
Now Support Windows's OS.

- 01_windows_set_language.exe 
    - Used to set yourown language !
- 02_windows_create_sample_one.exe
    - Used to create the trainning sample 
- 03_windows_create_sample_two.exe
    - Used to create the validation sample 
- 04_windows_create_index.exe
    - create the csv files for mapping the image_object under sample folder 
- 05_windows_train.exe
    - start to training the data and make .h5 model files
- 06_windows_predict.exe
        - start to make prediction for new images


 - Current , base on the .h5 files , delopers can load the .h5 files and make their own prediction coding by themself for the templer time. Just google how to load the h5 files,   and there you go !


---

## Demo Example
---
# 2020 Taiwan Present Image Classfication !
---

- put the image under sample folder with .jpg files.

    - ![](__image/01_Dir_Intro.jpg)

- 01 Set the language !

    - ![](__image/record_01_set_language.mp4)

- 02 Put the image object your want to train under __sample__ folder
-    Then, run the 02_exe, 03_exe files to create the samples.
    - ![](__image/record_02_create_samples.mp4)

- 03 Create the mapping index.csv and start training by keras-tensorflow packages.
    - ![](__image/record_03_cerate_index_et_train.mp4)

- 04 Let's Start to Predict ! 😈 
    - Put all your 🆕 images related to your object under 📂 demo_test/test/
    - Run the 06_windows_predict.exe by click
    - ![](__image/record_04_prediction.mp4)
    - Please be ✍️ that the program is running all images model by model
        - For example: 🥶🥶
        - it runs two images togther at 1st-model han and report  two images result (True or False)
        - then, it runs the same two images together at 2nd-model tsai and report two images result (True or False)
