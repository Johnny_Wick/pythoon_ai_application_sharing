## Chinese Remainder Theorem Application

---




- Referencd Theorem in wiki link [Link](https://en.wikipedia.org/wiki/Chinese_remainder_theorem)
- ![Theorem](image/photo_wiki.PNG)

-----

Application Description

- Input any number which are [__pairwise coprime__](https://en.wikipedia.org/wiki/Coprime_integers)

  (Take the quiz for example: we take 5, 6, 7, 11)

- The application will automatically calculate and return the all independent 'numbers' correspond such '5', '6', '7', '11'






- Then input the pratically result for:
    - Taking all soldier in 5 rows and remain which number  ?
    - Taking all soldier in 6 rows and remain which number  ?
    - Taking all soldier in 7 rows and remain which number  ?
    - Taking all soldier in 11 rows and remain which number ?

- Application will calculate the piossible soldiers it will be ... please check the demo_1


## Demo One 
---
![Demo_01](image/demo_01.mp4)




## Demo Two
---
Base on the chinese version's wiki page, verify the formula result is consist with wiki's content.
---

![Demo2_Image](image/demo2_image.jpg)

---

![Demo_02](image/demo_02.mp4)


cd 


### How to Run
---
- For Mac-OS:   
    - click mac_application             (Not implement yet .. 😓)
- For Windows:  
    - click windows_application         (Not implement yet .. 😓)
- For Linux with Python installed: type (Done  👌 )
    - python3 main.py

## Note:

- This application could run not only 4 pairwise numbers, just type as more as your praticall situation needs..
- For example: The estimate Order you want to measure is ~ ( m1*m2*m3*m4) , keep that in mind..


## Reference:
---

- Base on the famous physician's facebook post (Taiwan)

- Link Post here: 
- ![Quiz](image/photo_quiz.PNG)
