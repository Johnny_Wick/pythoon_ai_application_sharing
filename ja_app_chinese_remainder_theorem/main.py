




if __name__ == "__main__":



    # Obtenir Les n1 - nk
    msg =  "Base on the following formula , please input the numbers\n"
    msg += "Note: the '=' notation is different from wiki's notation.\n"
    msg += "---------------------------------------------------------\n"
    msg += "x '=' a1 (mod n1)\n"
    msg += "       ....      \n"
    msg += "       ....      \n"
    msg += "       ....      \n"
    msg += "x '=' ak (mod nk)\n"
    msg += "---------------------------------------------------------\n\n"
    print(msg)

    print("Please input n1 , n2 , ... nk one by one, then type 'q' for exit setting")
    n1_nk_list = []
    while True:
        print("Current [n1, n2, ... nk] = ", n1_nk_list)
        _input = input()
        if _input in ['q','Q']:
            break
        else:
            _input = int(_input)
            n1_nk_list.append(_input)
    
    total_multiply = 1
    for any_value in n1_nk_list:
        total_multiply = total_multiply * any_value


    print("\n\nIn your test result:")
    b1_bk_list = []
    for value in n1_nk_list:
        print(" X mod %d ,  the remainder is  ?"%value)
        bi = int(input())
        b1_bk_list.append(bi)
    

    # Obtenir M 
    m1_mk_list = []
    for any_value in n1_nk_list:
        _mi_ = int(total_multiply / any_value)
        m1_mk_list.append(_mi_)
    
    # Find b parameters
    print("\n\nFiind each  mi '=' 1 mod (ri * ni) ....")
    print("----------------------------------------")
    r1_rk_list = []
    for index, any_value in enumerate(n1_nk_list):
        for i in range(1,(any_value+1)):
            _bi_multiply_mi_ = i * m1_mk_list[index]
            remainder        = _bi_multiply_mi_ % any_value
            if remainder == 1:
                bi_x_mi = i*m1_mk_list[index]
                print(" %d * %d '=' 1 mod (%d)"%(i,m1_mk_list[index],any_value))
                r1_rk_list.append(i)
            else:
                pass
    print("----------------------------------------")


    print(n1_nk_list)
    print(m1_mk_list)
    print(r1_rk_list)


    # Formula:
    print("\n\n")
    print("X  '=' r1 * n1 * m1 + r2 * n2 * m2 + .......  mod (n1*n2*n3*n4 ...)" )
    msg = "X  '='  "
    for index , ni in enumerate(n1_nk_list):
        msg += " b%d*%d*%d + "%((index+1),r1_rk_list[index],m1_mk_list[index])

    msg = msg[:-2]
    msg += " mod  ("
    for ni in n1_nk_list:
        msg += "%d*"%ni
    msg = msg[:-1]
    msg += ")"
    print(msg)

    msg = "X  '=' "
    x = 0
    for index , ni in enumerate(n1_nk_list):
        msg += " %d*%d*%d + "%(b1_bk_list[index],r1_rk_list[index],m1_mk_list[index])
        x   += b1_bk_list[index] * r1_rk_list[index] * m1_mk_list[index]
    
    msg = msg[:-2]
    msg += " mod  (%d)"%total_multiply
    print(msg)


    # the x need to divide by total_multiply again 
    unique = x % total_multiply
    print("----------------------Get Formula----------------------")
    print("X   =     %d  +   number * %d   "%(unique,total_multiply))
    print("                  [number = 1, 2, 3, 4, ........ n]")

    print("\n---------------------------------------")
    print("Please input the number, e.g: 7")
    nb = int(input())
    print("X   = %d"%(unique+(nb*total_multiply)))
    print("----------------------------------------")



    

